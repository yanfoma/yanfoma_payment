Make a prepaid web payment account
1. customer pays
#who is paying
#to which store
#for what item
#what price is the item
#when is payment requested
#which currency
#which country
2. deduct from customer account
3. add to store account

Create a passwordless account from the store
#Customer email
#Customer phone number (main)
#Default Currency

Payment system
9 Steps

1. Customer send request for payment through PayStore
#who is paying
#to which merchant
#for what item
#what price is the item
#which payment method
#when is payment requested
#which currency
#which country

2. Merchant receives payment request from Customer
3. Merchant sends request to Yanfoma Payments
4. Yanfoma Payments receives the request from Merchant
5. Yanfoma Payments checks payment request information. If correct, proceed to the next stage. Else, reject and send error information to Merchant
6. Yanfoma Payments creates order and sends payment page to Merchant
7. Customer pays
8. Yanfoma Payments confirms payment to Merchant by updating payment status
9. Yanfoma Payments returns to merchant payment page with payment result
