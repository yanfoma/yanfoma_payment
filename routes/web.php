<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return "Yanfoma Payments API v1 is up and running ...";
});

  /*
    [
        "from" => "",
        "to" => "",
        "items" => "",
        "date" => "",
        "price" => ""
    ]
    */

Route::get('/send', 'OrderController@send');

Route::get('/register_passwordless_prepaid_account', 'OrderController@register_passwordless_prepaid_account');